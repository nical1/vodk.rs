vodk.rs
=======

This repository contains various things I am toying with including:

- vodk_math: A strongly typed vector/matrix library (needs a real name)
- lyon: A set of tools to tesselate paths
- vodk_id: Facilities to use strongly typed ids (I use them all over the place)
- vodk_gpu: some old junk you shouldn't use because glium is better.
- ...other junk not worth looking at.
